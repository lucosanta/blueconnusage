package it.unibo.tesi.bluetoothdatastructures;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MessageDataSource {

	// Database fields
		private SQLiteDatabase database;
		private MySQLiteMessage dbHelper;
		private String[] allColumns = { MySQLiteMessage.COLUMN_ID,
				MySQLiteMessage.COLUMN_MODE,
				MySQLiteMessage.COLUMN_DATA,  
				MySQLiteMessage.COLUMN_ORA,
				MySQLiteMessage.COLUMN_MITTENTE,
				MySQLiteMessage.COLUMN_DESTINATARIO,
				MySQLiteMessage.COLUMN_TEXT,
				MySQLiteMessage.COLUMN_MILLISSENT};

		public MessageDataSource(Context context) {
			dbHelper = new MySQLiteMessage(context);
		}

		public void open() throws SQLException {
			database = dbHelper.getWritableDatabase();
		}

		public void close() {
			dbHelper.close();
		}

		public BTMessage createMessage(BTMessage comments) {
			ContentValues values = new ContentValues();
			values.put(MySQLiteMessage.COLUMN_MODE, ""+comments.getMode());
			values.put(MySQLiteMessage.COLUMN_DATA, comments.getData());
			values.put(MySQLiteMessage.COLUMN_ORA, comments.getOra());
			values.put(MySQLiteMessage.COLUMN_MITTENTE, comments.getMittente());
			values.put(MySQLiteMessage.COLUMN_DESTINATARIO, comments.getDestinatario());
			values.put(MySQLiteMessage.COLUMN_TEXT, comments.getText());
			values.put(MySQLiteMessage.COLUMN_MILLISSENT, comments.getSendMillis());

			long insertId = database.insert(MySQLiteMessage.TABLE_COMMENTS, null,values);
			Cursor cursor = database.query(MySQLiteMessage.TABLE_COMMENTS,
					allColumns, MySQLiteMessage.COLUMN_ID + " = " + insertId, null,
					null, null, null);
			cursor.moveToFirst();
			BTMessage newMessage = cursorToComment(cursor);
			cursor.close();
			return newMessage;
		}

		public void deleteUser(BTMessage comment) {
			long id = comment.getId();
			System.out.println("Comment deleted with id: " + id);
			database.delete(MySQLiteMessage.TABLE_COMMENTS, MySQLiteMessage.COLUMN_ID
					+ " = " + id, null);
		}

		public List<BTMessage> getAllComments() {
			List<BTMessage> comments = new ArrayList<BTMessage>();

			Cursor cursor = database.query(MySQLiteMessage.TABLE_COMMENTS,
					allColumns, null, null, null, null, null);

			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				BTMessage comment = cursorToComment(cursor);
				comments.add(comment);
				cursor.moveToNext();
			}
			// Make sure to close the cursor
			cursor.close();
			return comments;
		}

		private BTMessage cursorToComment(Cursor cursor) {
			BTMessage message = new BTMessage();
			message.setId(cursor.getLong(0));
			message.setMode(Integer.parseInt(cursor.getString(1)));
			message.setData(cursor.getString(2));
			message.setOra(cursor.getString(3));
			message.setMittente(cursor.getString(4));
			message.setDestinatario(cursor.getString(5));
			message.setText(cursor.getString(6));
			message.setSendMillis(cursor.getString(7));
			
			return message;
		}
}
