package it.unibo.tesi.bluetoothdatastructures;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteConversation extends SQLiteOpenHelper {

	public static final String TABLE_COMMENTS = "conversation";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_DESTINATARIO="destinatario";
	public static final String COLUMN_MODE="modal";


	private static final String DATABASE_NAME = "conversation.db";
	private static final int DATABASE_VERSION = 1;

	//Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_COMMENTS + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_DESTINATARIO
			+ " text not null, "+ COLUMN_MODE
			+ " text not null);";

	public MySQLiteConversation(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteConversation.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
		onCreate(db);
	}

} 