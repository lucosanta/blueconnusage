package it.unibo.tesi.bluetoothdatastructures;

public class BTMessage {
	
	public static final int BTMESSAGE_MESSAGE=0;
	public static final int BTMESSAGE_FLASHLIGHT=1;
	public static final int BTMESSAGE_VIBRATE=2;
	public static final int BTMESSAGE_REQUESTLISTMANAGECONN=3;
	public static final int BTMESSAGE_RESPONSELISTMANAGECONN=4;
	/*-----------Possono essere messi anche tanti altri---------------*/
	
	
	long id;
	
	
	String sendMillis,data,ora,mittente,destinatario,text;
	int mode;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSendMillis() {
		return sendMillis;
	}

	public void setSendMillis(String sendMillis) {
		this.sendMillis = sendMillis;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOra() {
		return ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public String getMittente() {
		return mittente;
	}

	public void setMittente(String mittente) {
		this.mittente = mittente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	
}
