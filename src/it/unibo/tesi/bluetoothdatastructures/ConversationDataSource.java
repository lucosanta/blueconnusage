package it.unibo.tesi.bluetoothdatastructures;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ConversationDataSource {
	// Database fields
	private SQLiteDatabase database;
	private MySQLiteConversation dbHelper;
	private String[] allColumns = { MySQLiteConversation.COLUMN_ID,
			MySQLiteConversation.COLUMN_DESTINATARIO,  
			MySQLiteConversation.COLUMN_MODE};

	public ConversationDataSource(Context context) {
		dbHelper = new MySQLiteConversation(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Conversation createMessage(Conversation comments) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteConversation.COLUMN_DESTINATARIO, comments.getDestinatario());
		values.put(MySQLiteConversation.COLUMN_MODE, comments.getMode());

		long insertId = database.insert(MySQLiteConversation.TABLE_COMMENTS, null,values);
		Cursor cursor = database.query(MySQLiteConversation.TABLE_COMMENTS,
				allColumns, MySQLiteConversation.COLUMN_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		Conversation newMessage = cursorToComment(cursor);
		cursor.close();
		return newMessage;
	}

	public void deleteUser(BTMessage comment) {
		long id = comment.getId();
		System.out.println("Comment deleted with id: " + id);
		database.delete(MySQLiteConversation.TABLE_COMMENTS, MySQLiteConversation.COLUMN_ID
				+ " = " + id, null);
	}

	public List<Conversation> getAllComments() {
		List<Conversation> comments = new ArrayList<Conversation>();

		Cursor cursor = database.query(MySQLiteConversation.TABLE_COMMENTS, allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Conversation comment = cursorToComment(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return comments;
	}

	private Conversation cursorToComment(Cursor cursor) {
		Conversation conversation = new Conversation();
		conversation.setId(cursor.getLong(0));
		conversation.setDestinatario(cursor.getString(1));
		conversation.setMode(cursor.getString(2));
		return conversation;
	}
}
