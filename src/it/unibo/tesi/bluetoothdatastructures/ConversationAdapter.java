package it.unibo.tesi.bluetoothdatastructures;

import it.tesi.unibo.blueconnusage.R;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ConversationAdapter extends ArrayAdapter<Conversation>{

	class ViewHolder{
		ImageView imv;
		TextView name;
	}
	protected int resource; 
	protected LayoutInflater inflater;

	String exercise ;
	protected Context context;

	public ConversationAdapter(Context context, int resourceId,List<Conversation> manageThreads) {
		super(context, resourceId, manageThreads);
		this.context=context;
		this.resource=resourceId;
		this.inflater=LayoutInflater.from(context);
	}

	@Override
	public View getView(int position,View v,ViewGroup parent){
		Conversation person=getItem(position);
		ViewHolder holder;
		if(v==null){
			v=inflater.inflate(resource, parent,false);
			holder=new ViewHolder();
			
			holder.imv= (ImageView)v.findViewById(R.id.imageView1);
			holder.name=(TextView)v.findViewById(R.id.destinatario);
			v.setTag(holder);
		} else{
			holder=(ViewHolder)v.getTag();
		}
		
		holder.name.setText(person.getDestinatario());
		if(person.getDestinatario().equals("Globale")){
			holder.imv.setImageResource(R.drawable.world);
		}
		return v;
	}
	
	


}