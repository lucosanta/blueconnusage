package it.unibo.tesi.bluetoothdatastructures;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteMessage extends SQLiteOpenHelper {

	public static final String TABLE_COMMENTS = "message";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MODE = "modalita";
	public static final String COLUMN_DATA = "data";
	public static final String COLUMN_ORA = "ora";
	public static final String COLUMN_MITTENTE = "mittente";
	public static final String COLUMN_DESTINATARIO="destinatario";
	public static final String COLUMN_TEXT="text";
	public static final String COLUMN_MILLISSENT="millisSent";


	private static final String DATABASE_NAME = "message.db";
	private static final int DATABASE_VERSION = 1;

	//Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_COMMENTS + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_MODE
			+ " text not null, "+ COLUMN_DATA
			+ " text not null, "+ COLUMN_ORA
			+ " text not null, "+ COLUMN_MITTENTE
			+ " text not null, "+ COLUMN_DESTINATARIO
			+ " text not null, "+ COLUMN_TEXT
			+ " text not null, "+ COLUMN_MILLISSENT
			+ " text not null);";

	public MySQLiteMessage(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteMessage.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
		onCreate(db);
	}

} 

