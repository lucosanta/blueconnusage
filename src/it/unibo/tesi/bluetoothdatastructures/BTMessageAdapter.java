package it.unibo.tesi.bluetoothdatastructures;

import it.tesi.unibo.blueconnusage.MessageActivity;
import it.tesi.unibo.blueconnusage.R;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BTMessageAdapter extends ArrayAdapter<BTMessage>{

	class ViewHolder{
		TextView mittente;
		TextView text;
		TextView ora;
	}
	protected int resource; 
	protected LayoutInflater inflater;

	String exercise ;
	protected Context context;

	public BTMessageAdapter(Context context, int resourceId,List<BTMessage> manageThreads) {
		super(context, resourceId, manageThreads);
		this.context=context;
		this.resource=resourceId;
		this.inflater=LayoutInflater.from(context);
	}

	@Override
	public View getView(int position,View v,ViewGroup parent){
		BTMessage person=getItem(position);
		ViewHolder holder;
		if(v==null){
			v=inflater.inflate(resource, parent,false);
			holder=new ViewHolder();
			
			holder.ora=(TextView)v.findViewById(R.id.ora);
			holder.mittente=(TextView)v.findViewById(R.id.mittente);
			holder.text=(TextView)v.findViewById(R.id.text);
			v.setTag(holder);
		} else{
			holder=(ViewHolder)v.getTag();
		}
		
		holder.ora.setText(person.getData()+" "+person.getOra());
		String[] split=person.getMittente().split("/");
		
		holder.mittente.setText(split[0]);
		
		if(person.getMittente().equals(MessageActivity.getMyName())){
			v.setBackgroundResource(R.drawable.bluebutton);
		}
		holder.text.setText(person.getText());
		return v;
	}
}