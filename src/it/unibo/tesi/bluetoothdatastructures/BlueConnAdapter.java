package it.unibo.tesi.bluetoothdatastructures;

import it.tesi.unibo.blueconnapi.R;
import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConnectionDevice;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BlueConnAdapter extends ArrayAdapter<BlueConnectionDevice>{

	class ViewHolder{
		TextView name;
		TextView address;
		ProgressBar signalPower;
		TextView activated;
	}
	protected int resource; 
	protected LayoutInflater inflater;

	String exercise ;
	BlueConn blue;
	protected Context context;

	public BlueConnAdapter(Context context, int resourceId,List<BlueConnectionDevice> manageThreads,BlueConn blue) {
		super(context, resourceId, manageThreads);
		this.context=context;
		this.blue=blue;
		this.resource=resourceId;
		this.inflater=LayoutInflater.from(context);
	}

	@Override
	public View getView(int position,View v,ViewGroup parent){
		IBlueConnectionDevice person=getItem(position);
		ViewHolder holder;
		if(v==null){
			v=inflater.inflate(resource, parent,false);
			holder=new ViewHolder();
			holder.name=(TextView)v.findViewById(R.id.device);
			holder.address=(TextView)v.findViewById(R.id.rssiLevel);
			holder.signalPower = (ProgressBar) v.findViewById(R.id.signalpower);
			holder.activated=(TextView)v.findViewById(R.id.activated);
			holder.signalPower.setMax(100);
			holder.signalPower.setProgress(0);
			holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_greenlevel));

			v.setTag(holder);
		} else{
			holder=(ViewHolder)v.getTag();
		}

		holder.name.setText(person.getDevice().getName());
		holder.address.setText(person.getDevice().getAddress());
		//
		Integer rssi;
		if(person == null ){

		} else {
			if(person.getRssi()== null) {
				rssi=0;
			} else {
				rssi= Integer.parseInt(person.getRssi());
			}


			if(rssi == 0){
				holder.signalPower.setProgress(0);
				//holder.rssi.setText("No signal");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_greenlevel));
			} else if(rssi< 0 && rssi>=-20){
				holder.signalPower.setProgress(100);
				//holder.rssi.setText("Ottimo");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_greenlevel));
			}else if(rssi<-20 && rssi>= -40){
				holder.signalPower.setProgress(100);
				//holder.rssi.setText("Molto buono");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_greenlevel));
			} else if(rssi<-40 && rssi>= -55){
				holder.signalPower.setProgress(75);
				//holder.rssi.setText("Buono");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_yellgreen));
			} else if(rssi<-55 && rssi>=-70){
				holder.signalPower.setProgress(50);
				//holder.rssi.setText("Discreto");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_yellow));
			} else if(rssi<-70 && rssi >= -100){
				holder.signalPower.setProgress(25);
				//holder.rssi.setText("Scarso");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_red));
			} else {
				holder.signalPower.setProgress(0);
				//holder.rssi.setText("No signal");
				holder.signalPower.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_yellgreen));
			}


			holder.activated.setText(person.getMode());
		}
		return v;
	}
}