package it.unibo.tesi.bluetoothdatastructures;


import it.tesi.unibo.blueconnapi.library.threads.Manage;
import it.tesi.unibo.blueconnusage.R;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BluetoothDeviceListAdapter extends ArrayAdapter<Manage>{

	class ViewHolder{
		TextView name;
		TextView address;
	}
	protected int resource; 
	protected LayoutInflater inflater;

	String exercise ;
	protected Context context;

	public BluetoothDeviceListAdapter(Context context, int resourceId,List<Manage> manageThreads) {
		super(context, resourceId, manageThreads);
		this.context=context;
		this.resource=resourceId;
		this.inflater=LayoutInflater.from(context);
	}

	@Override
	public View getView(int position,View v,ViewGroup parent){
		Manage person=getItem(position);
		ViewHolder holder;
		if(v==null){
			v=inflater.inflate(resource, parent,false);
			holder=new ViewHolder();
			holder.name=(TextView)v.findViewById(R.id.name);
			holder.address = (TextView)v.findViewById(R.id.address);				
			v.setTag(holder);
		} else{
			holder=(ViewHolder)v.getTag();
		}

		holder.name.setText(person.getBluetoothDeviceConnected().getName());
		holder.address.setText(person.getBluetoothDeviceConnected().getAddress());
		return v;
	}
}
