package it.tesi.unibo.blueconnusage;

import java.util.List;
import java.util.UUID;

import it.tesi.unibo.blueconnapi.library.system.BlueConn;
import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import it.unibo.tesi.bluetoothdatastructures.BlueConnAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;



////////////
/**
 * Example of a standard activity which use BlueConnAPI 
 * 
 * 
 * @author santonastasi
 *
 */
public class MainActivity extends Activity {

	//------------------------------------------------
	//Constants
	public final static String TAG="MainActivity";
	public final static boolean LOG_PRESENT=true;
	
	public static final UUID[] uuids = {
		UUID.fromString("f783e78a-4390-40a4-bc6a-2d2fa9160c66"),
		UUID.fromString("63886203-a801-44da-9063-0eef6c0b0fb8"), 
		UUID.fromString("3c970000-5ee6-4246-b3ee-f2404e7aaced"),
		UUID.fromString("9b3a2f36-d0ee-4d04-8598-3cad098b5fcd"),
		UUID.fromString("4908c9a4-4a5b-49f5-b6d9-3cdcaa99e4bc")};
	
	//------------------------------------------------
	//Global variables
	static BlueConn blueConn;
	Button start;
	static EditText log;
	Handler blueConnHandler;
	static BlueConnAdapter blueAdapter;
	static List<BlueConnectionDevice> bcdList;
	ActionBar action;
	static Activity act;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);

		act=this;
		action=this.getActionBar();
		blueConnHandler= new MyBlueConnHandler();
		
		//create a new BlueConn object
		blueConn= new BlueConn(this,(BlueConnHandler) blueConnHandler,uuids);
		
		//create a button
		start= (Button) this.findViewById(R.id.start);
		start.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// start blueConn services
				blueConn.start(0x3000);
				
			}
			
		});
		
		//create a text field
		log= (EditText) findViewById(R.id.log);
		
		//create a list example with SimpleBlueConnAdapter
		bcdList= blueConn.whoAreMyNeighbor();
		blueAdapter = new BlueConnAdapter(this, R.layout.item_device, bcdList, blueConn);
		blueAdapter.setNotifyOnChange(true);
		
		ListView list = (ListView) findViewById(R.id.listNeighbor);
		list.setAdapter(blueAdapter);
		
		list.setOnItemClickListener(blueConn.deviceListener);
		
		
		
		
		
		
	}
	
	//--------------------------------------------------
	// BlueConnHandler 
	
	public static class MyBlueConnHandler extends BlueConnHandler{
		
		@Override
		public void handleMessage(Message msg){
			switch(msg.what){
			case BlueConnHandler.MSG_ERROR:
				if(LOG_PRESENT) log.setText("ERROR"+":"+msg.obj+"\n"+log.getText().toString());
				
				break;
				
			case BlueConnHandler.MSG_REFRESH:
				if(LOG_PRESENT) log.setText("REFRESH"+":"+msg.arg1+"\n"+log.getText().toString());
				//bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();
				break;
				
			/*case BlueConnHandler.MSG_FORWARD:
				String fwd= new String((byte[]) msg.obj,0,msg.arg1);
				if(LOG_PRESENT) log.setText("FORWARD"+":"+fwd +"\n"+log.getText().toString());
				break;*/
				
			case BlueConnHandler.MSG_WRITE:
				String write= new String((byte[]) msg.obj,0,msg.arg1);
				if(LOG_PRESENT) log.setText("WRITE: "+write+"\n"+ log.getText().toString());
				break;
				
			case BlueConnHandler.MSG_READ:
				String read= new String((byte[]) msg.obj,0,msg.arg1);
				if(LOG_PRESENT) log.setText("READ"+":"+read+"\n"+log.getText().toString());
				break;
				
				
			case BlueConnHandler.MSG_ACCEPTINGTHREAD:
				if(LOG_PRESENT) log.setText("ACCEPTING"+":"+msg.obj+"\n"+log.getText().toString());
				break;
				
			case BlueConnHandler.MSG_CONNECTINGTHREAD:
				if(LOG_PRESENT) log.setText("CONNECTING"+":"+msg.obj+"\n"+log.getText().toString());
				
				break;
				
			case BlueConnHandler.MSG_NEWNEIGHBOR:
				//bcdList= blueConn.whoAreMyNeighbor();
				if(LOG_PRESENT) log.setText("NEIGHBOR"+": new neighbor"+"\n"+log.getText().toString());
				blueAdapter.notifyDataSetChanged();
				break;
				
			case BlueConnHandler.MSG_ONDISCOVERY:
				act.setProgressBarIndeterminateVisibility(Boolean.TRUE); 
				break;
			
			case BlueConnHandler.MSG_DISCOVERYENDED:
				act.setProgressBarIndeterminateVisibility(Boolean.FALSE); 
				break;
				
			case BlueConnHandler.MSG_RSSIREFRESH:
				if(LOG_PRESENT) log.setText("REFRESH RSSI"+"\n"+log.getText().toString());
				//bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();
				break;
			
				
			case BlueConnHandler.MSG_GOODBYENEIGHBOR:
				//bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();
				break;
				
			case BlueConnHandler.MSG_DEBUG:
				if(LOG_PRESENT) log.setText("DEBUG"+":"+msg.obj+"\n"+log.getText().toString());
				break;
			
			}
		}
		
	
	}
	
	
	
	//-------------------------------------------------
	// Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		blueConn.unregisterBlueReceiver();
	}
}
