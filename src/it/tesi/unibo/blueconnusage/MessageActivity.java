package it.tesi.unibo.blueconnusage;


import it.tesi.unibo.blueconnapi.library.system.BlueConn;
import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConn;
import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import it.tesi.unibo.blueconnusage.FragmentConversation.OnHeadlineSelectedListener;
import it.unibo.tesi.bluetoothdatastructures.BTMessage;
import it.unibo.tesi.bluetoothdatastructures.BlueConnAdapter;
import it.unibo.tesi.bluetoothdatastructures.Conversation;
import it.unibo.tesi.bluetoothdatastructures.ConversationDataSource;
import it.unibo.tesi.bluetoothdatastructures.MessageDataSource;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MessageActivity extends FragmentActivity implements OnHeadlineSelectedListener{

	static final boolean D= true;
	static final boolean T= true;
	static final String TAG="MainActivity";
	static final int REQUEST_CONNECT_DEVICE=1;
	static final int REQUEST_ENABLE_BLUETOOTH=2;
	static MessageActivity main;

	public static final UUID[] uuids = {
		UUID.fromString("f783e78a-4390-40a4-bc6a-2d2fa9160c66"),
		UUID.fromString("63886203-a801-44da-9063-0eef6c0b0fb8"), 
		UUID.fromString("3c970000-5ee6-4246-b3ee-f2404e7aaced"),
		UUID.fromString("9b3a2f36-d0ee-4d04-8598-3cad098b5fcd"),
		UUID.fromString("4908c9a4-4a5b-49f5-b6d9-3cdcaa99e4bc")};

	ConversationDataSource cds;
	Conversation c;
	List<Conversation> listConv;
	static String name;
	AlertDialog.Builder alertDialog;
	public Button button1,button2;
	//---------------------------------------------------------------------

	static BlueConn blueConn;
	Button start;
	static EditText log;
	Handler blueConnHandler;
	static BlueConnAdapter blueAdapter;
	static List<BlueConnectionDevice> bcdList;
	ActionBar action;
	static Activity act;

	//
	static HashMap<String,Boolean> checkNewMessage= new HashMap<String,Boolean>();
	static HashMap<String,Integer> numberOfNewMessage= new HashMap<String,Integer>();

	//---------------------------------------------------------------------


	@Override
	public void onCreate(Bundle b) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		main=this;
		act=this;
		if(D) Log.d("DEBUG", "Initializing...");
		super.onCreate(b);
		this.setContentView(R.layout.message_main);
		act.setProgressBarIndeterminateVisibility(Boolean.FALSE); 

		cds= new ConversationDataSource(main);
		cds.open();
		listConv=cds.getAllComments();
		cds.close();

		if(listConv.size()==0){
			cds.open();
			c=new Conversation();
			c.setId(1);
			c.setDestinatario("Globale");
			c.setMode("Pubblica");

			cds.createMessage(c);
			cds.close();
		}

		blueConnHandler= new MyBlueConnHandler();

		//create a new BlueConn object
		blueConn= new BlueConn(this,(BlueConnHandler) blueConnHandler,uuids);

		// Create an instance of ExampleFragment
		FragmentConversation firstFragment = new FragmentConversation();

		// In case this activity was started with special instructions from an Intent,
		// pass the Intent's extras to the fragment as arguments
		firstFragment.setArguments(getIntent().getExtras());

		// Add the fragment to the 'fragment_container' FrameLayout
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();


		start = (Button) this.findViewById(R.id.start);
		start.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// start blueConn services
				blueConn.start(0x3000);

			}

		});

	}


	@Override
	public void onArticleSelected(int position) {
		// TODO Auto-generated method stub
		// Capture the article fragment from the activity layout
		BluetoothDevice device = blueConn.whoAreMyNeighbor().get(position).getDevice();
		FragmentMessage messageFrag = new FragmentMessage();
		FragmentConversation conv= (FragmentConversation) this.getSupportFragmentManager().findFragmentById(R.id.fragment_container);
		Bundle args = new Bundle();
		if(D) Log.d("MAINACTIVITY", ""+position);
		args.putInt(FragmentMessage.ARG_POSITION, position);
		args.putString("Destinatario", device.getName()+"/"+device.getAddress());
		messageFrag.setArguments(args);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.hide(conv);
		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack so the user can navigate back
		transaction.replace(R.id.fragment_container, messageFrag);
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
		//}

	}


	public static MessageActivity getInstance(){
		return main;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.delete:
			MessageDataSource mds= new MessageDataSource(this);

			mds.open();
			List<BTMessage> list= mds.getAllComments();
			for(int i=0;i<list.size();i++){
				mds.deleteUser(list.get(i));
			}

			mds.close();
			if(FragmentMessage.getInstance() == null ){


			} else {
				FragmentMessage f=FragmentMessage.getInstance();
				f.updateList();
			}
			return true;
		case R.id.sendall:
			sendAll();
			return true;


		}
		return false;
	}

	public static String getMyName(){
		return name;
	}

	public static class MyBlueConnHandler extends BlueConnHandler{

		@Override
		public void handleMessage(Message msg){
			switch(msg.what){
			case BlueConnHandler.MSG_ERROR:

				break;

			case BlueConnHandler.MSG_REFRESH:
				/*bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();*/
				FragmentConversation.getInstance().updateList();
				break;

				/*
			case BlueConnHandler.MSG_FORWARD:
				String fwd= new String((byte[]) msg.obj,0,msg.arg1);
				break;
				 */

			case BlueConnHandler.MSG_WRITE:
				Log.d("3- Write", "Message sent");

				String write= new String((byte[]) msg.obj,0,msg.arg1);

				BTMessage mW = MessageActivity.parseJsonToMessage(write);
				MessageDataSource mdsW= new MessageDataSource(main);
				mdsW.open();
				mdsW.createMessage(mW);
				mdsW.close();
				if(FragmentMessage.getInstance() == null){


				} else {
					FragmentMessage.getInstance().updateList();
				}


				break;

			case BlueConnHandler.MSG_READ:
				String read= new String((byte[]) msg.obj,0,msg.arg1);
				Log.d(TAG,"READ "+read);
				BTMessage mR=MessageActivity.parseJsonToMessage(read);				
				////
				/*ConversationDataSource cds = new ConversationDataSource(main);
				cds.open();
				List<Conversation> convList= cds.getAllComments();

				boolean present=false;
				for(int ai=0;ai<convList.size();ai++){
					if(m.getDestinatario().equals(convList.get(ai).getDestinatario())){
						present=true;
					}
				}
				if(!present){
					Conversation con = new Conversation();
					con.setDestinatario(m.getDestinatario());
					con.setMode("Privata");
					cds.createMessage(con);
				} 
				cds.close();

				if(FragmentConversation.getInstance() == null){

				} else {
					FragmentConversation.getInstance().updateList();
				}
				 */
				/////

				if(FragmentConversation.getInstance() == null){

				} else {
					//ListView list = FragmentConversation.getInstance().getListView();
					checkNewMessage.put(mR.getMittente(), true);
					if(numberOfNewMessage.containsKey(mR.getMittente())){
						int num=numberOfNewMessage.get(mR.getMittente());
						numberOfNewMessage.put(mR.getMittente(), ++num );
						FragmentConversation.said=false;
					} else {
						numberOfNewMessage.put(mR.getMittente(), 1 );
						FragmentConversation.said=false;
					}
					FragmentConversation.getInstance().updateList();
				}
				MessageDataSource mdsR= new MessageDataSource(main);
				mdsR.open();
				mdsR.createMessage(mR);
				mdsR.close();

				if(FragmentMessage.getInstance() == null ){

				} else {
					FragmentMessage.getInstance().updateList();
				}


				break;
				/*
			case BlueConnHandler.MSG_NEWCONNECTION:
			case BlueConnHandler.MSG_POLICYCHANGED:
			case BlueConnHandler.MSG_ACCEPTINGTHREAD:
			case BlueConnHandler.MSG_CONNECTINGTHREAD:
				 */
			case BlueConnHandler.MSG_NEWNEIGHBOR:
				/*bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();*/
				FragmentConversation.getInstance().updateList();

				break;

			case BlueConnHandler.MSG_ONDISCOVERY:
				act.setProgressBarIndeterminateVisibility(Boolean.TRUE); 
				break;

			case BlueConnHandler.MSG_DISCOVERYENDED:
				act.setProgressBarIndeterminateVisibility(Boolean.FALSE); 
				break;

			case BlueConnHandler.MSG_RSSIREFRESH:
				/*bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();*/
				FragmentConversation.getInstance().updateList();

				break;


			case BlueConnHandler.MSG_GOODBYENEIGHBOR:
				/*bcdList= blueConn.whoAreMyNeighbor();
				blueAdapter.notifyDataSetChanged();*/
				FragmentConversation.getInstance().updateList();

				break;

			case BlueConnHandler.MSG_STATECONNECTING:
				Toast.makeText(main, "Connecting..", Toast.LENGTH_SHORT).show();
				break;

			case BlueConnHandler.MSG_STATECONNECTED:
				if(FragmentConversation.getInstance() == null){

				} else {
					FragmentConversation.getInstance().updateList();
				}
				Toast.makeText(main, "Connected", Toast.LENGTH_SHORT).show();
				break;
			case BlueConnHandler.MSG_DEBUG:
				break;

			}
		}


	}
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static BTMessage parseJsonToMessage(String json){
		BTMessage m= new BTMessage();
		Gson gson=new Gson();
		m=gson.fromJson(json, m.getClass());
		return m;
	}

	/**
	 * 
	 * @param m
	 * @return
	 */
	public static String parseMessageToJson(BTMessage m){
		String json="";
		Gson gson= new Gson();
		json=gson.toJson(m);
		return json;
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		blueConn.clear();
		blueConn=null;

	}





	public void sendAll(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Send all");
		alert.setMessage("Send a text message to all of your neighbors");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				
				
				
				Calendar c= Calendar.getInstance();
				int day=c.get(Calendar.DAY_OF_MONTH);
				int month=(c.get(Calendar.MONTH)+1);
				int year=c.get(Calendar.YEAR);
				int hour=c.get(Calendar.HOUR_OF_DAY);
				int minute=c.get(Calendar.MINUTE);
				String hours,minutes,days,months;
				if(hour<10){
					hours="0"+hour;
				} else {
					hours=""+hour;
				}
				if(minute<10){
					minutes="0"+minute;
				} else {
					minutes=""+minute;
				}
				if(day<10){
					days="0"+day;
				} else {
					days=""+day;
				}
				if(month<10){
					months="0"+month;
				} else {
					months=""+month;
				}					

				String dayofyear= days+"/"+months+"/"+year;
				String ora= hours+":"+minutes;
				String millis=""+c.getTimeInMillis();
				//String destinatario=socket.getRemoteDevice().getName()+"/"+socket.getRemoteDevice().getAddress();
				String mittente=BlueConn.bluetoothAdapter.getName()+"/"+BlueConn.bluetoothAdapter.getAddress();

				BTMessage m=new BTMessage();
				m.setMode(0);
				m.setData(dayofyear);
				m.setOra(ora);
				m.setSendMillis(millis);
				m.setMittente(mittente);
				m.setDestinatario("Globale");
				m.setText(value);
				String json=MessageActivity.parseMessageToJson(m);
				blueConn.write(json,"Globale",IBlueConn.WRITE_BROADCAST);

			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}



	public void writeToAll(){

	}


	public void highlight(){

	}
}

