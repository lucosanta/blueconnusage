package it.tesi.unibo.blueconnusage;

import java.util.List;

import it.tesi.unibo.blueconnapi.library.system.BlueConn;
import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import it.unibo.tesi.bluetoothdatastructures.BlueConnAdapter;
import it.unibo.tesi.bluetoothdatastructures.Conversation;
import it.unibo.tesi.bluetoothdatastructures.ConversationAdapter;
import it.unibo.tesi.bluetoothdatastructures.ConversationDataSource;
import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class FragmentConversation extends ListFragment {
	OnHeadlineSelectedListener mCallback;

	ConversationDataSource cds;
	Conversation c;
	List<Conversation> listConv;
	ConversationAdapter adapter;
	public static FragmentConversation fragConv;
	public static boolean said = false;

	// The container Activity must implement this interface so the frag can deliver messages
	public interface OnHeadlineSelectedListener {
		/** Called by HeadlinesFragment when a list item is selected */
		public void onArticleSelected(int position);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		fragConv= this;
		final ActionBar actionBar= this.getActivity().getActionBar();
		actionBar.setTitle("Device");
		actionBar.setSubtitle("Choose somebody to talk");



		updateList();


	}

	public static FragmentConversation getInstance(){
		return fragConv;
	}

	List<BlueConnectionDevice> bcdList;
	BlueConnAdapter blueAdapter;

	public void updateList() {
		/*cds= new ConversationDataSource(this.getActivity());
		cds.open();
		listConv=cds.getAllComments();
		cds.close();
		adapter=new ConversationAdapter(getActivity(), R.layout.item_conversation, listConv);
		// Create an array adapter for the list view, using the Ipsum headlines array
		setListAdapter(adapter);*/

		
		
		bcdList= MessageActivity.blueConn.whoAreMyNeighbor();
		blueAdapter = new BlueConnAdapter(this.getActivity(), R.layout.item_device, bcdList, MessageActivity.blueConn);
		blueAdapter.setNotifyOnChange(true);
		setListAdapter(blueAdapter);
		for(int i = 0 ; i< bcdList.size(); i++){
			String name = bcdList.get(i).getDevice().getName()+"/"+bcdList.get(i).getDevice().getAddress();
			if(MessageActivity.checkNewMessage.containsKey(name)){
				if(MessageActivity.numberOfNewMessage.containsKey(name)){
					if(!said){
						said=true;

						Toast.makeText(getActivity(), MessageActivity.numberOfNewMessage.get(name)+" new messages from "+name, Toast.LENGTH_SHORT ).show();
					}
				}
			}
			
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();

		// When in two-pane layout, set the listview to highlight the selected list item
		// (We do this during onStart because at the point the listview is available.)
		/*if (getFragmentManager().findFragmentById(R.id.message_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }*/
		final ActionBar actionBar= this.getActivity().getActionBar();
		actionBar.setTitle("Conversation");
		actionBar.setSubtitle("Choose somebody to talk");
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			mCallback = (OnHeadlineSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		BluetoothDevice device = MessageActivity.blueConn.whoAreMyNeighbor().get(position).getDevice();
		//// TODO: Handler connecting change
		if(MessageActivity.blueConn.whoAreMyNeighbor().get(position).getMode().equals(BlueConnectionDevice.MODE_INACTIVE)){
			Toast.makeText(getActivity(), "Not active now", Toast.LENGTH_SHORT).show();
		} else {
			BlueConnectionDevice bcd= new BlueConnectionDevice();
			bcd.setDevice(device);
			bcd.setMode(BlueConnectionDevice.MODE_CONNECTING);
			bcd.setRssi( MessageActivity.blueConn.whoAreMyNeighbor().get(position).getRssi());
			MessageActivity.blueConn.whoAreMyNeighbor().set(position,bcd);
			if(MessageActivity.numberOfNewMessage.containsKey(device.getName()+"/"+device.getAddress())){
				MessageActivity.numberOfNewMessage.remove(device.getName()+"/"+device.getAddress());
			}
			//MessageActivity.handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTING).sendToTarget();
			////
			BlueConn.run.executeConnect(device, BlueConn.bluetoothAdapter);
		}
		

		// Notify the parent activity of selected item

		mCallback.onArticleSelected(position);

		// Set the item as checked to be highlighted when in two-pane layout
		getListView().setItemChecked(position, true);
	}
}