package it.tesi.unibo.blueconnusage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConn;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;
import it.tesi.unibo.blueconnapi.library.threads.Runnables;
import it.unibo.tesi.bluetoothdatastructures.BTMessage;
import it.unibo.tesi.bluetoothdatastructures.BTMessageAdapter;
import it.unibo.tesi.bluetoothdatastructures.Conversation;
import it.unibo.tesi.bluetoothdatastructures.MessageDataSource;
import android.app.ActionBar;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class FragmentMessage extends Fragment {
	final static String ARG_POSITION = "position";
	final static String TAG="FRAGMESSAGE";
	int mCurrentPosition = -1;
	List<BTMessage> listTotMessage,listMessageMitt,listMessageMittProvv,listMessageRic,listConversationMessage;
	List<Conversation> listConv;
	MessageActivity main;
	BluetoothAdapter adapter;
	View rootView;
	Button buttonSend;
	EditText e;
	boolean active=true;
	boolean global=false;
	public static FragmentMessage frag;
	ListView list;
	BTMessageAdapter adapterMessList;
	String destinatario;
	EditText et1;
	Button send;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView =inflater.inflate(R.layout.fragment_message, container, false);
		// If activity recreated (such as from screen rotate), restore
		// the previous article selection set by onSaveInstanceState().
		// This is primarily necessary when in the two-pane layout.
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);

		}

		mCurrentPosition= this.getArguments().getInt(ARG_POSITION);
		destinatario = this.getArguments().getString("Destinatario");

		adapter = BlueConn.bluetoothAdapter;
		listMessageMitt= new ArrayList<BTMessage>();
		listMessageMittProvv= new ArrayList<BTMessage>();
		listMessageRic= new ArrayList<BTMessage>();
		listConversationMessage= new ArrayList<BTMessage>();
		listTotMessage= new ArrayList<BTMessage>();
		frag=this;


		buttonSend= (Button) rootView.findViewById(R.id.send);
		e=(EditText) rootView.findViewById(R.id.editText1);
		main=MessageActivity.getInstance();

		buttonSend.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {

				Calendar c= Calendar.getInstance();
				int day=c.get(Calendar.DAY_OF_MONTH);
				int month=(c.get(Calendar.MONTH)+1);
				int year=c.get(Calendar.YEAR);
				int hour=c.get(Calendar.HOUR_OF_DAY);
				int minute=c.get(Calendar.MINUTE);
				String hours,minutes,days,months;
				if(hour<10){
					hours="0"+hour;
				} else {
					hours=""+hour;
				}
				if(minute<10){
					minutes="0"+minute;
				} else {
					minutes=""+minute;
				}
				if(day<10){
					days="0"+day;
				} else {
					days=""+day;
				}
				if(month<10){
					months="0"+month;
				} else {
					months=""+month;
				}					

				String dayofyear= days+"/"+months+"/"+year;
				String ora= hours+":"+minutes;
				String millis=""+c.getTimeInMillis();
				//String destinatario=socket.getRemoteDevice().getName()+"/"+socket.getRemoteDevice().getAddress();
				String mittente=BlueConn.bluetoothAdapter.getName()+"/"+BlueConn.bluetoothAdapter.getAddress();

				BTMessage m=new BTMessage();
				m.setMode(0);
				m.setData(dayofyear);
				m.setOra(ora);
				m.setSendMillis(millis);
				m.setMittente(mittente);
				m.setDestinatario(destinatario);
				m.setText(e.getText().toString());

				String json=MessageActivity.parseMessageToJson(m);
				Log.d("1 - Write", "json");
				MessageActivity.blueConn.write(json, destinatario, IBlueConn.WRITE_SINGLEPEER);

			}

		});

		final ActionBar actionBar= this.getActivity().getActionBar();


		String[] split= destinatario.split("/");
		String name= split[0];
		String address=split[1];
		actionBar.setTitle(name);
		actionBar.setSubtitle(address);
		updateList();


		// Inflate the layout for this fragment
		return rootView;
	}

	public BTMessageAdapter getMessAdapter(){
		return this.adapterMessList;
	}

	public static FragmentMessage getInstance(){
		return frag;
	}

	@Override
	public void onStart() {
		super.onStart();

		// During startup, check if there are arguments passed to the fragment.
		// onStart is a good place to do this because the layout has already been
		// applied to the fragment at this point so we can safely call the method
		// below that sets the article text.
		Bundle args = getArguments();
		if (args != null) {
			// Set article based on argument passed in
			updateArticleView(args.getInt(ARG_POSITION));
		} else if (mCurrentPosition != -1) {
			// Set article based on saved instance state defined during onCreateView
			updateArticleView(mCurrentPosition);
		}
	}

	public void updateArticleView(int position) {
		/*TextView article = (TextView) getActivity().findViewById(R.id.article);
        article.setText(Ipsum.Articles[position]);*/
		mCurrentPosition = position;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// Save the current article selection in case we need to recreate the fragment
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}

	public void onDestroy(){
		super.onDestroy();
		//BlueConn.run.getManageThreadsList().get(i);
		for(int i = 0; i< Runnables.manageThreads.size(); i++){
			String name= Runnables.manageThreads.get(i).getBluetoothDeviceConnected().getName()+"/"+Runnables.manageThreads.get(i).getBluetoothDeviceConnected().getAddress();
			if(name.equals(destinatario)){
				Runnables.manageThreads.get(i).cancel();
			}
		}
		frag=null;
	}

	public void updateList() {
		listMessageRic.clear();
		listConversationMessage.clear();
		listMessageMitt.clear();
		listMessageRic.clear();
		listMessageMittProvv.clear();
		listTotMessage.clear();

		MessageDataSource mds= new MessageDataSource(this.getActivity());


		mds.open();
		listTotMessage=mds.getAllComments();
		mds.close();

		for(int i=0;i<listTotMessage.size();i++){


			if(listTotMessage.get(i).getMittente().equals(adapter.getName()+"/"+adapter.getAddress())
					&& listTotMessage.get(i).getDestinatario().equals(destinatario)){
				listMessageMitt.add(listTotMessage.get(i));
				//listMessageMittProvv.add(listTotMessage.get(i));
			}
			if(listTotMessage.get(i).getDestinatario().equals(adapter.getName()+"/"+adapter.getAddress()) 
					|| listTotMessage.get(i).getDestinatario().equals("Globale")
					&& listTotMessage.get(i).getMittente().equals(destinatario)){
				listMessageMitt.add(listTotMessage.get(i));
				//listMessageRic.add(listTotMessage.get(i));
			}
		}


		if(MessageActivity.D) Toast.makeText(main, ""+listMessageMitt.size(), Toast.LENGTH_SHORT).show();
		listMessageMitt.addAll(listMessageRic);
		listConversationMessage = listMessageMitt;
		listMessageMitt=listMessageMittProvv;

		/*for(int i=0;i<listConversationMessage.size();i++){
			long millisMittA = Long.parseLong(listConversationMessage.get(i).getSendMillis());
			for(int j=0;j<listConversationMessage.size();j++){
				long millisMittB = Long.parseLong(listConversationMessage.get(i).getSendMillis());
				if (j==i){

				} else {
					if(millisMittB<millisMittA){
						BTMessage tmp1 =listConversationMessage.get(i);
						BTMessage tmp2 =listConversationMessage.get(j);
						listConversationMessage.set(i, tmp2);
						listConversationMessage.set(j, tmp1);
					}
				}
			}
		}*/
		adapterMessList= new BTMessageAdapter(main, R.layout.item_message, listConversationMessage);

		list=(ListView) rootView.findViewById(R.id.log);
		list.setAdapter(adapterMessList);
		adapterMessList.setNotifyOnChange(true);


	}

}