# README #

This README is for a little and easy example of the usage of BlueConnAPI, which is a chat using Bluetooth connection.

* Before starting using this project make sure you have installed ADT or Android Plugin for Eclipse. 
* Then you have to download BlueConnAPI and import project.
* Once you have done, click Run 
* N.B. don't try the application with Android emulator because it doesn't support Bluetooth connection

* For issue contact at luca.santonastasi@gmail.com